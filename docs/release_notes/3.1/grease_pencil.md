# Blender 3.1: Grease Pencil

## Line Art

![](../../images/Line_Art_collection_inverse.gif){style="width:400px;"}

- New option to toggle collection inverse selection. This allows
  conveniently exclude a collection and select "the rest of" the scene.
  (blender/blender@40c8e23d481c)
  - Differences between collection line art usage flag:
    1.  Collection line art usage is global, used to specify e.g. if a
        collection should appear in calculation at all.
    2.  Invert collection selection is used to easily select a
        collection and "everything else other than that collection",
        making it convenient to set up such cases.

![](../../images/Line_Art_Facemark_Contour_Preserving.png){style="width:400px;"}

- New option to preserve contour lines when using face mark filtering.
  (blender/blender@dde997086ce2)

![](../../images/Line_art_noise_tolerant_chaining.png){style="width:400px;"}

- Noise tolerant chaining. This feature takes advantage of original line
  art chain which is a continuous long chain, instead of splitting it at
  each occlusion change, this function tolerates short segments of
  "zig-zag" occlusion incoherence and don't split the chain at these
  points, thus creates a much smoother result.
  (blender/blender@5ae76fae90da)

<!-- -->

- Back face culling option.
  (blender/blender@579e8ebe79a1)

## Operators

- New Merge All Layers option in Merge operator.
  (blender/blender@556c71a84ac1)
- New option in PDF export to export full scene frame range.
  (blender/blender@e1c4e5df225d)

## Tools

- Now Fill tool allows to use a Dilate negative value to contract the
  filled area and create a gap between fill and stroke.
  (blender/blender@3b1422488195)

![](../../images/Fill_Dilate-Contract_sample.png){style="width:400px;"}

## Modifiers and VFX

- New Shrinkwrap modifier.
  (blender/blender@459af75d1ed5)
- New Randomize parameter in Length modifier.
  (blender/blender@a90c35646766)
