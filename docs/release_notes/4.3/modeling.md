# Blender 4.3: Modeling & UV

## Copy Material to Selected

`Copy Material to Selected` now sets the `Link` type of the selected objects to match those of the active object. Without this, it would change the underlying material slots on the mesh data block, which is probably not what the user intends. With the new behavior, it now changes the selected objects to use object-linked materials (blender/blender@2b9c7b3e8f).
