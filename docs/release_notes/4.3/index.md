# Blender 4.3 Release Notes

Blender 4.3 is currently in **Alpha**. Phase **Bcon1** until August 28,
2024.
[See schedule](https://projects.blender.org/blender/blender/milestone/21).

Under development in [`main`](https://projects.blender.org/blender/blender/src/branch/main).

* [Animation & Rigging](animation_rigging.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Nodes & Physics](nodes_physics.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [Sequencer](sequencer.md)
* [User Interface](user_interface.md)
* [Extensions](extensions.md)
