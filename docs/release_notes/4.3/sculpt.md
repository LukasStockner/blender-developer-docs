# Blender 4.3: Sculpt, Paint, Texture

## Brushes

> INFO **Under Development**
> This feature is [available for testing in a branch](https://devtalk.blender.org/t/brush-assets-prototype-feedback/33568), but not in 4.3 yet.

Brushes are now stored in asset libraries shared across projects, instead of having copies in each blend file.

The built-in brushes are in the Essentials library shipped with Blender. Brushes can be created by duplicating an existing built-in brush. Each brush is saved in its own blend file in the user asset library, with textures and images packed.

The asset shelf is the new place to select brushes, instead of the toolbar. Brushes are organized into asset catalogs, instead of being grouped by tool.

The asset shelf appears horizontally across the bottom of the 3D viewport in sculpt and paint modes. It can be configured to show a subset of catalogs, depending on the needs of the project. Alternatively, brushes can be selected in a popup from the tool settings.

### Converting Brushes

To make existing brushes available, save them in a blend file in the user asset library folder. By default this folder is `Documents/Blender/Assets`. Then in the outliner with Blender File view, right click each brush and Mark as Asset.
-->