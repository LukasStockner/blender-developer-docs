# Blender 4.3: Compositor

## Removed

- The *Auto Render* option is now removed. (blender/blender@cbabe2d3ef)