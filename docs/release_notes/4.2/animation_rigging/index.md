# Blender 4.2: Animation & Rigging

## Motion Paths
- Motion Path points now use the Vertex theme color (blender/blender@fc025abde6).
- You can now specify a custom color for anything before or after the current frame. Previously this was a single color (blender/blender@883af6dd63).

## Animation Editors
- New operator in the View (regular + pie) menu: "Frame Scene Range" (blender/blender@95966813ae58).
  This changes the horizontal view to match the scene range. If the [preview range][preview-range]
  is active, the operator will be re-labeled as "Frame Preview Range" and zoom to that instead. This
  was added to the Dope Sheet, Timeline, Graph Editor, NLA Editor, and Video Sequence Editor.

[preview-range]: https://docs.blender.org/manual/en/4.2/editors/timeline.html#frame-controls

## Graph Editor
- Modified the way the Ease operator works. It now creates an S-Curve shape that can be moved to
  favor either the left or the right end. In addition to that, a "Sharpness" property was added to
  control how abrupt the change is. Pressing `Tab` while running the operator toggles which property the slider affects. (blender/blender@62f140e048,
  [Manual](https://docs.blender.org/manual/en/4.2/editors/graph_editor/fcurves/editing.html#ease)).
- The drawing code has been optimized resulting in smoother panning in heavy scenes. (blender/blender@3eda9619f9, blender/blender@0157fede80)
- Performance when moving keys in heavy scenes has been drastically improved. Previous Blender
  versions could hang for minutes while it is instantaneous in 4.2. The more keys the scene has the
  more pronounced the effect is.(blender/blender@8a79212031, blender/blender@6a55a126e9,
  blender/blender@c6c7d3d8c4, blender/blender@08de3fa0b6)

<figure>
<video src="graph_editor_speedup.mp4" title="Performance comparison between 3.6 and 4.2" width="720" controls=""></video>
<figcaption>Comparison of moving 300 keys in a 10,000 frame file 5 frames to the right.</figcaption>
</figure>

## Dope Sheet

![Keys of the new 'Generated' type, between regular keys. Some of the 'generated' keys are selected, to show the color difference.](key_type_generated_dark_theme.webp)

- New key type: 'Generated' (blender/blender@51e1f29a68). This new key type indicates that the key
  was set by some automated tool (like an add-on), rather than manually by an animator. It is drawn
  smaller & dimmed in the Dope Sheet. These keys can still be operated on like any other, including
  changing their key type to something else.
- It is now possible to see and edit non relative shape key animations in the "Shape Key Editor". (blender/blender@7ff8a7b173)


## NLA
- Performance of the editor has been improved when only viewing a section of a long action
  (blender/blender@24b4461be3).
- Update NLA "Make Singe User" operator modal text.
 (blender/blender@298c9276e9)

## Armatures
- Subdividing a bone names the new bones in sequential order (blender/blender@9f0cc7fe64).
  Subdividing "Bone" makes "Bone.001" its first child, "Bone.002" the next, etc. Previously this
  would have been "Bone", "Bone.003", "Bone.002", "Bone.001".
- It is now possible to specify a wire width for custom bone shapes. This setting is per bone and can be found under the "Custom Shape" options of the bone (blender/blender@f9ea64b0ba).

## Constraints
- The behavior of the "Limit Rotation" constraint has been modified to fix a bug ([Bug Report](https://projects.blender.org/blender/blender/issues/117927), blender/blender@ed2408400d). In older versions the constrained object would flip back to the minimum when an angle is higher than 180º. This is now changed so values higher than 180º are supported. This means a minimum of 90º and a maximum of 270º now works properly. Also instead of snapping back to the minimum, the rotation will snap to the closest boundary when the rotation is out of the allowed range. Rigs relying on the old behavior of snapping back to the minimum might not work as expected.

## Drivers
- A new "Copy Driver to Selected" menu item is now available in the right-click
  menu of driven properties (blender/blender@bd3518946e).  It copies the
  property's driver to the same property on all selected items (as long as they
  also have that property). It's essentially the same as the "Copy to Selected"
  feature, but copies drivers instead of values.

## Copy Global Transform add-on

Copy Global Transform ([manual][copy-global-transform-doc]) has gotten two new, related features
(blender/blender-addons@0b1d563a2f4f04477229d77944581c205671813f):

[copy-global-transform-doc]: https://docs.blender.org/manual/en/latest/addons/animation/copy_global_transform.html

![Copy Global Transform: Fix to Camera and Relative Copy-Paste](copy_global_transform_panel.webp)

### Fix to Camera

Also known as "bake to camera", this operator will ensure selected objects/bones remain static
(relative to the camera) on unkeyed frames.

This is done by generating new keys. These keys will be of type 'Generated' so that it remains clear
which keys were manually created, and which were generated. This way the tool can be re-run to
re-generate the keys.

It operates on the scene frame range, or on the preview range if that is active.

### Relative Copy-Paste

The "Relative" panel has copy/paste buttons that work relative to some object. That object can also
be chosen in the panel. If no object is chosen, the copy/paste will happen relative to the active
scene camera.

## Pose Library

Added an option to "Blend Pose Flipped" from the context menu. Previously this was only available during modal operations. (blender/blender@3a40d2813f)

Pressing `Ctrl` before dragging on a Pose Library item now starts it in flipped mode. Previously this was only possible once the blending operation has started. (blender/blender@345cd70404)

## Python API
- `rna_struct.keyframe_insert()` now has a new, optional keyword argument `keytype` to set the key type
  (blender/blender@1315991ecbd179bbff8fd08469dda61ec830d72f). This makes it possible to set the new
  key's type directly. Example: `bpy.context.object.keyframe_insert("location", keytype='JITTER')`.
- There is a new function on the F-Curve: `bake`. This creates keys at a given interval for a given range on the existing curve
  (blender/blender@20a4c9c928).
