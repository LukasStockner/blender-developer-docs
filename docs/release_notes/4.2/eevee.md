# Blender 4.2: EEVEE

The EEVEE render engine was rewritten to allow deeper changes, removing long standing limitations and facilitating future evolution.

## Migrating from older version
Most of the scenes should be automatically adjusted to keep the same look. However, there are some cases that need manual adjustments.
See [migration process](eevee_migration.md) for a more details.

## Global Illumination
EEVEE now uses screen space ray tracing for every BSDF. There is no longer any limitation to the number of BSDFs.

Material using `Blended` render method are now removed from reflections instead of being projected onto the background.

## Lights
There is no longer a limit to the number of lights in a scene. However, only 4096 lights can be visible at the same time.

- Lights are now visible through refractive surfaces. A new transmission influence factor has been added and set to 0 for older files.
- Lights now support ray visibility options.
- Glossy lighting no longer leaks at the back of objects.

## Shadows
Shadows are now rendered using Virtual Shadow Maps. This greatly increases the maximum resolution, reduces biases and simplifies the setup.

- Light visibility is now computed using Shadow Map Ray Tracing, providing plausible soft shadows without needing shadow jittering.
- Shadow Map biases have been removed and computed automatically. A workaround for the shadow terminator issue is under development.
- Contact shadows have been removed since Virtual Shadow Maps are precise enough in most cases.
- Shadow clip start has been removed and replaced by an automatic value.

## Shading
### Shading modes
`Material > Blend Mode` has been replaced by `Material > Render Method`. `Blended` corresponds to the former `Alpha Blend`.
`Material > Shadow Mode` has been replaced by `Object > Visibility > Ray Visibility > Shadow` at the object level.

A `Backface Culling` option for shadow as well as a `Transparent Shadows` option was added to reduce the performance impact of rendering Virtual Shadow Maps.

For both shadow and surfaces in `Dithered` render method, the behavior of transparency is the same as with former `Alpha Hashed`. To reproduce the same behavior as former `Alpha Clip`, materials need be modified by adding `Math` node with `Greater Than` mode.

`Blended` materials now have correct rendering ordering of their opaque pixels.

The `Screen-Space Refraction` option was renamed `Raytraced Transmission` and affects all transmissive BSDF nodes (Translucent BSDF, Subsurface Scattering and Refraction BSDF, etc.).

### Displacement
Displacement is now supported with the exception of the `Displacement Only` mode which falls back to `Displacement And Bump`.

### Thickness
A new Thickness output has been introduced. This allows better modeling of Refraction, Subsurface Scattering and Translucency. Some material might need adjustment to keep the same appearance. This replaces the former `Refraction Depth`. (blender/blender!120384)

### Subsurface Scattering
The new Subsurface Scattering implementation now supports any number of BSSRDF nodes with arbitrary radii.
Subsurface Scattering now doesn't leak between objects and have no energy loss.

The subsurface translucency is now always computed and the associated option has been removed.

## Light-Probes
The render panel options have been split between the light probe data panel and the world data panel.

### Volume Light-Probes
The new volume light probes baking now converges faster with higher resolution.

Baking is now done at the object level, allowing volume light probes to be edited after baking and linked from other files. Optionally they can bake sky visibility to allow dynamic sky lighting.

There is a new rejection algorithm and flood fill to reduce shadow leaking.

Volume light probes now affect volumetric. However object volumes and world volumes are not yet captured by the volume light probes.

### Sphere Light Probes
Sphere light probes are now dynamic and are updated as they move.
Filtered version for rough surfaces are now firefly-free. The look of the reflections can appear less "foggy" than in the previous version of EEVEE.

## Volume
World volume now completely block distant light (world and sun lights). Older file can be converted using the conversion operator in the help menu or in the `World > Volume` panel. (blender/blender!114062)

- Volume lighting is now dithered to avoid flickering.
- EEVEE now maximizes the depth range automatically if no world is present.
- Mesh objects now have correct volume intersection instead of rendering bounding boxes.
- Evaluation of many small volume objects have been optimized.

## World
Sun light contribution is now automatically extracted from HDRI lighting.

## Image stability
Viewport image stability has been improved using velocity-aware temporal supersampling.

Clamping options have seen their behavior unified with Cycles. They are still independant settings.

## Motion Blur
Motion blur is now partially supported in the viewport, through the camera view.

Added support for Shutter Curve.

## Depth of Field
Depth of field was rewritten and optimized, removing the `Denoise Amount` and `High Quality Slight Defocus` settings which are now always on.

## UI
- The panels and properties have been adjusted to be closer to Cycles.
- The object `Display As` property set to `Wire` or `Bounds` will now render in `Render` viewport shading mode. (blender/blender@1f2b935146)
- Cycles and EEVEE now share the `Cast Shadow` property.

## Multithreaded Shader Compilation
There's a new `Max Shader Compilation Subprocesses` option in `Preferences > System > Memory & Limits` that allows using multiple subprocesses to speed-up shader compilation on Windows and Linux.