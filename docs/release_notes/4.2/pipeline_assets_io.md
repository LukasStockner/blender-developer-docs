# Blender 4.2: Import & Export

## Collection Exporters
File format exporters can now be associated with Collections. One or
more exporters can be added and configured in the Collection properties
panel. Settings are stored in .blend files for easy sharing and persistence
across Blender sessions. (blender/blender@509a7870c3570cbf8496bcee0c94cdc1e9e41df5)

This feature streamlines the process of re-exporting the same asset(s)
repeatedly. For example when creating glTF assets for a game and
iterating on the look, or when using Blender in a studio pipeline to
create USD assets.

All collections in the scene can be exported with a single
click through the File menu. Additionally, each collection can be
exported to multiple file formats simultaneously.

![](./images/io_collection_export.png){style="width:400px;"}

## USD
- The new hair curves object type is now supported for both import and export. (blender/blender@ea256346a89, blender/blender@21db0daa4e5)
- Point cloud import is now supported. (blender/blender@e4ef0f6ff4fe1df8188a85e2a6b33e62801085e3)
- Unicode names are now supported on import and are optional for export. Only software using USD 24.03 or greater can support Unicode files. (blender/blender@9ad2c7df0b8f)
- New Import options
  - Import only defined prims or all (blender/blender@bfa54f22ceb)
  - Convert USD dome light to a world shader (blender/blender@e1a6749b3d9)
  - Validate meshes on import (blender/blender@2548132e23f)
- New Export options
  - Filter which types to export (blender/blender@a6a5fd053a3cd036ac06028dc11c5b4c37e7935a)
  - Convert the world shader to a USD dome light (blender/blender@e1a6749b3d9)
  - Stage up axis (blender/blender@24153800618)
  - XForm operator convention (blender/blender@36f1a4f94f3)
  - Triangulate meshes (blender/blender@b2d1979882f)
  - Down-sample exported textures for USDZ (blender/blender@3e73b9caa5a)
  - Generate MaterialX network from Blender shader nodes. Supports a subset of all shader nodes and their functionality. (blender/blender!122575)

<!--
- Other notable improvements ...
-->

## Alembic
- The new hair curves object type is now supported for both import and export. (blender/blender@ea256346a89, blender/blender@b24ac18130e)
- Addressed long-standing issue where animated curves would not update during render. (blender/blender@ea256346a89)
- Import multiple files at once. (blender/blender!121492)

## Collada
The built-in support for this format is now [considered legacy](https://devtalk.blender.org/t/moving-collada-i-o-to-legacy-status/34621). It will be removed in a future version. (blender/blender@82f9501a4a, blender/blender-manual@20e4984ad6)

## Validation

OBJ, PLY, STL importers now default to the Validate Meshes option being on.
This makes import process a bit slower, but prevents later crashes in case input files
contain malformed data. Validation itself is now 2-3x faster compared to previous versions. 
(blender/blender@761dd6c9238, blender/blender!121413).