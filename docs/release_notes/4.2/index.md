# Blender 4.2 LTS Release Notes

Blender 4.2 LTS is currently in **Beta**. Phase **Bcon3** until July 10,
2024.
[See schedule](https://projects.blender.org/blender/blender/milestone/19).

Under development in [`blender-v4.2-release`](https://projects.blender.org/blender/blender/src/branch/blender-v4.2-release).

* [Animation & Rigging](animation_rigging/index.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE](eevee.md)
* [Import & Export](pipeline_assets_io.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Nodes & Physics](nodes_physics.md)
* [Python API](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [Sequencer](sequencer.md)
* [User Interface](user_interface.md)
* [Extensions](extensions.md)

## Compatibility

On Windows and Linux a CPU with SSE4.2 is now required.
This is supported since AMD Bulldozer (2011) and Intel Nehalem (2008).

### IDProperties

System-defined and library-overridable IDProperties [are now statically typed](python_api.md#statically-typed-idproperties).

### Add-ons
Most add-ons that used to ship with Blender, are now available on the [Extensions Platform](https://extensions.blender.org/) where you can browse, install, and update them online from within Blender. Read more about [Extensions](extensions.md).

Add-ons that haven't been converted to extensions yet, are still supported as [legacy add-ons](https://docs.blender.org/manual/en/4.2/extensions/addons.html#legacy-vs-extension-add-ons) and can be [installed from disk](https://docs.blender.org/manual/en/4.2/editors/preferences/extensions.html#install) as usual.

To ease the distribution and transition to the new system, a bundle of the add-ons that shipped with Blender 4.1 is available for [download](https://www.blender.org/download/release/Blender4.2/add-ons-legacy-bundle.zip).
