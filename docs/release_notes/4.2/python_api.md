# Blender 4.2: Python API

## Add-on Packaging

With the [new extensions platform](extensions.md), there is also a new method for packaging add-ons. It uses a separate `blender_manifest.toml` file instead of `bl_info` embedded in the script. See the [extensions user manual](https://docs.blender.org/manual/en/4.2/extensions/getting_started.html) for details.

The old packaging still works but is considered legacy, and add-ons are recommended to switch to the new manifest file.

## Additions

### Online Access

- `bpy.app.online_access` is a read-only property to check whether Blender (and add-ons) is expected to access the internet.
- Add-ons are expected to check for this property before accessing the internet. See [documentation](https://docs.blender.org/manual/en/dev/advanced/extensions/addons.html#internet-access).
- Also available is `bpy.app.online_access_override` which tells whether Blender was launched with `--offline-mode` / `--online-mode`.

### `bpy.utils` Module

- The `EXTENSIONS` option has been added to `bpy.utils.user_resource()`
  (blender/blender@dc9430c48039bda36c1ba052f00c20e554f6aef0).
- Add `bpy.utils.register_cli_command` & `unregister_cli_command`
  so scripts can define their own command line functionality via (`-c`, `--command`).
  (blender/blender@9372e0dfe092e45cc17b36140d0a3182d9747833)

### Animation

- `rna_struct.keyframe_insert()` now has a new, optional keyword argument `keytype` to set the key
  type (blender/blender@1315991ecbd179bbff8fd08469dda61ec830d72f).
  This makes it possible to set the new key's type directly.
  Example: `bpy.context.object.keyframe_insert("location", keytype='JITTER')`.

### Layout Panels

Added support for layout panels for operator properties drawing, in popups, redo panel and the file browser sidebar. Add-ons are recommended to use panels for organizing properties now, to follow Blender user interface conventions. (blender/blender@aa03646a74, blender/blender@02681bd44d285b51ffe9cf6d3df71af1980a6f70)

### Exporters

Exporter add-ons can add support for the new [Collection Exporters](pipeline_assets_io.md#collection-exporters). This requires a [`FileHandler`](https://docs.blender.org/api/main/bpy.types.FileHandler.html) with `bl_export_operator`, and a string property named `collection` on the operator to indicate the collection to export. See the FBX and glTF add-ons for an example.

### Modal Operators

- `Window.modal_operators` provides a list of running modal operators. (blender/blender#122193)
- Modal operator can add `MODAL_PRIORITY` to `bl_options` to handle events before other modal operators. (blender/blender@6efd7a6a507c2691d2de72ced9ea9577a68a4f3b)

### Handlers
- Render: include info string for "render_stats" handler (blender/blender@fcc481a40746014465bc2a6c025abd5ee4b0e3fa)

## Breaking changes

### Statically Typed IDProperties

IDProperties have never be statically typed before, modifying their type was (way) too easy from Python. A mere assignment of a float value to an IDProperty from Python code would e.g. convert that property to a 'double' one. This was especially problematic for system-defined  IDProperties (i.e. these used as backend storage for internal Blender data, like geometry node modifier properties, or Python-defined 'dynamic RNA' properties). It would also easily break library overrides, which cannot give useful results when the type of the reference and override data differs.

The following IDProperties now have a fixed, static type that cannot be modified from the python API. In array (vector) cases, the length is also considered as part of the type, and therefore fixed.

- System-defined:
  - Geometry Node modifier settings IDProperties.
  - Python-defined properties (through the `bpy.props.` module).
- User-defined:
  - Properties created from the UI (`Custom Properties` panel, in the `Properties` editor), when their `Library Overridable` option is enabled.

When assigning a value to such property, a 'reasonable conversion' is attempted if possible (e.g. an integer can be converted to a float, but a float cannot be assigned to an integer IDProperty), otherwise the assignment fails.

!!! note

    Technically, this is a breaking change. In particular in Geometry Nodes modifier case, where the 'expected' way to access these data from python is currently through 'raw' IDProperty path. However, practically, no 'valid' usages of the IDProperties should be broken by it.

Further details can be found in the [design task](https://projects.blender.org/blender/blender/issues/122743).

Commits: blender/blender@4d1fe98604, blender/blender@b278e37f70, blender/blender@76f03eb141, blender/blender@9c41bf4fa2


### `bpy.utils` Module

- The `AUTOSAVE` option has been removed from `bpy.utils.user_resource()`
  (blender/blender@6bfc8612bf757f24fe1abaa03dbf4bc194cfae4d).

### Render Settings

- Motion Blur settings have been de-duplicated between Cycles and EEVEE and moved to
  `bpy.types.RenderSettings`. (blender/blender@74b8f99b43)
  - `scene.cycles.motion_blur_position` -> `scene.render.motion_blur_position`
  - `scene.eevee.use_motion_blur` -> `scene.render.user_motion_blur`
  - `scene.eevee.motion_blur_position` -> `scene.render.motion_blur_position`
  - `scene.eevee.motion_blur_shutter` -> `scene.render.motion_blur_shutter`
- Light cast shadow setting have been de-duplicated between Cycles and EEVEE and moved to
  `bpy.types.Light`. (blender/blender@1036d9bdb2) 
  - `light.cycles.cast_shadow` -> `light.use_shadow`
- Bloom has been deprecated in EEVEE. (blender/blender#123059)
  - `view_layer.eevee.use_pass_bloom`
  - `scene.eevee.use_bloom`
  - `scene.eevee.bloom_threshold`
  - `scene.eevee.bloom_color`
  - `scene.eevee.bloom_knee`
  - `scene.eevee.bloom_radius`
  - `scene.eevee.bloom_clamp`
  - `scene.eevee.bloom_intensity`
- EEVEE (pre 4.2) specific Material settings have been deprecated or aliased where possible. (blender/blender@c7807a425a56a93a62420c264258fa94d18aba3d)
  - `bpy.types.Material.blend_method` => `bpy.types.Material.surface_render_method`. 'Opaque' and 'Alpha Clip' maps to deferred.
  - `show_transparent_back` has been renamed to `use_transparency_overlap`.
  - `use_screen_refraction` has been renamed to `use_raytrace_refraction`.
  - `use_sss_translucency` and `use_sss_translucency` have been deprecated.
- Light probe API has been changed. (blender/blender@6a71a91b83e5ec2174267b03865595cc7ad4ad5b)
  - Each light probe type now has its own type ()
  - `grid_` prefix has been removed and access has been limited to volume probes.
  - Parallax and other sphere probe only properties are be limited to sphere probes.
  - `visibility` properties have been deprecated.
- Deprecated shadow settings
  - `shadow_cube_size` and `shadow_cascade_size` are not used anymore and will be removed in a future release.
  
### Nodes

- Unused `parent` argument removed from the `NodeTreeInterface.new_panel` function
  for creating node group panels. (blender/blender#118792)
- Some node properties have been renamed to fix name collisions
  (blender/blender@deb332601c2a5c5c41df21543c08ac1381ca4a0a):
  - Compositor Box/Ellipse Mask node: `width` -> `mask_width` (same for `height`)
  - Shader AOV Output node: `name` -> `aov_name`
  - Geometry Color node: `color` -> `value`

### Image Object Operators

- The "add image object" operator has been deduplicated and unified into one operator:
  `object.empty_image_add`. (blender/blender@013cd3d1ba)<br>
  Previously there were two operators to add image empties
  (`object.load_reference_image` and `object.load_background_image`)
  with a separate operator for dropping (`object.drop_named_image`).
- The "add/remove background image from camera" operator has been renamed to clarify that this is
  only for the camera object (blender/blender@013cd3d1ba):
  - `view3d.background_image_add` -> `view3d.camera_background_image_add`.
  - `view3d.background_image_remove` -> `view3d.camera_background_image_remove`.