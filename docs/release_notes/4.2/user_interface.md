# Blender 4.2: User Interface

## General

- Support for horizontal rules in vertical layouts, vertical rules in horizontal.
  (blender/blender@4c50e6992f)
- Improved user interface scaling.
  (blender/blender@85762d99ef, blender/blender@f85989f5db, blender/blender@196cd48aad,
  blender/blender@23aaf88233, blender/blender@1f1fbda3ee)
- Icon changes. (blender/blender@42e7a720c9, blender/blender@4e303751ff, blender/blender@7b601464c3)
- Optimizations to the font shader. (blender/blender@a05adbef28)
- Interface font updated to Inter 4.0. (blender/blender@f235990009)

![Inter font](images/Inter.png){style="width:400px;"}

- Corrected coloring when hovering inactive UI items. (blender/blender@5bed08fc56)
- Tooltip changes. (blender/blender@3d85765e14, blender/blender@08cd80a284)
- Improved square color picker when shown very wide. (blender/blender@857341a7a7)
- Tooltips for colors featuring large sample and values. (blender/blender@41bbbd2359)

![Color tooltips](images/ColorTooltip.png){style="width:400px;"}

- Asset Browser now supports Maximize Area. (blender/blender@2d5a83f294)
- Wavelength node now uses nanometer-scale inputs. (blender/blender@cc541f2c07)

## Status Bar

- Improvements to Status Bar keymap display. (blender/blender@c4e5a70e07, blender/blender@44606c5e4d, blender/blender@42a8947eb1, blender/blender@dfe060ea6f, blender/blender@03cd8b359b, blender/blender@3fbc603137, blender/blender@1391b7de41, blender/blender@02798f8438)

![Status bar keymap](images/StatusBarKeymap.png){style="width:650px;"}

- Status Bar now collapses multiple X/Y/Z operations to a more concise format. (blender/blender@49bd285529)

![Status bar keymap](images/XYZCollapse.png){style="width:400px;"}

- Expanded Status Bar icons for keymap events. (blender/blender@d1b6621903)

![Keymap icons](images/KeymapIcons.png){style="width:500px;"}

- Tighter Status Bar keymap spacing. (blender/blender@65bfae2258)
- Status Bar mouse event icons are better aligned. (blender/blender@9cb298f706)

## Dialogs

- Improved large confirmation dialogs.
  (blender/blender@00b4fdce2c, blender/blender@6618755912, blender/blender@3bacd93ba6,
  blender/blender@2c8a3243ee, blender/blender@5eda7c9fd3, blender/blender@311e0270dd,
  blender/blender@d3798970199a6b58efc3701785829d17561ea352)
  
![Large confirmation](images/LargeConfirmation.png){style="width:500px;"}
  
- Improved small confirmation dialogs. (blender/blender@443ea628c5, blender/blender@529b8786f8)

![Small confirmation](images/SmallConfirmation.png){style="width:400px;"}

- Improved property dialogs.
  (blender/blender@8fee7c5fc7, blender/blender@da378e4d30, blender/blender@615edb3f3c)
- Panels now supported in dialog popups. (blender/blender@aa03646a74)
- Some operator confirmations removed. (blender/blender@6dd0f6627e, blender/blender@7226b52728)
- Improved confirmation when reverting the current file. (blender/blender@75a9cbed24, blender/blender@8735a8a6be)

## Menus
- Recent Items context menu now supports opening file location. (blender/blender@6789a88107)
- "Select" menus reorganized to be more consistent. (blender/blender@ede675705e, blender/blender@4dbca26b5c)
- Menus in mesh sculpt mode were reorganized after gaining new items (blender/blender@1fe8fbd93a)
- Changes to menu separator padding. (blender/blender@48dee674f3)
- All Zoom menus now use a new consistent format. (blender/blender@7e2075b809)

![Zoom menus](images/ZoomMenu.png)

## 3D Viewport

- Ability to toggle Camera Guides in Viewport Overlays. (blender/blender@392ac52ebb)
- Ability to toggle Camera Passepartout in Viewport Overlays. (blender/blender@2a287dc23c)
- Corrected initial viewport tilt. (blender/blender@913acaf395)
- More improvements to overlay text contrast. (blender/blender@3582e52f9c, blender/blender@3ee8a75e8e)
- Reduced dead zone for 3Dconnexion SpaceMouse. (blender/blender@d739d27a2d)
- Object menu gains a Modifiers submenu. (blender/blender@e63c8bb3c2)
- Sidebar made a bit wider. (blender/blender@068e624ebc)

## Properties Editor

- Add, remove, apply, and reorder modifiers of all selected objects by holding alt. (blender/blender@9a7f4a3b58d9)
- Changes to Modifer pinning. (blender/blender@7ee189416c)
- Add eyedropper button to camera focus distance. (blender/blender@7f8d4df410)

![Focus distance eyedropper](images/FocalDistancePicker.png){style="width:400px;"}

## Text Editor

- GLSL syntax highlighting. (blender/blender@462c144f414343ffbbac3546f1fae2bbf0bd52db)
- Drop strings & URLs into the text editor and Python console
  (blender/blender@64843cb12d543f22be636fd69e9512f995c93510)
![Drag URLs and texts to text and console editors](images/DragToTextAndConsole.png){style="width:500px;"}

## Outliner

- Ctrl-F can initiate Outliner filtering. (blender/blender@f5fdda776c)
- Orphan Data mode renamed to Unused Data. (blender/blender@72be662af4)
- Blender File mode now supports managing user counts. (blender/blender@d8f6ae7919)

![Blender File mode](images/OutlinerFileMode.png){style="width:500px;"}

## Unused Datablocks

The Purge operator now pops up a dialog where user can choose which options to apply,
and get instant feedback on the amounts of unused data-blocks will be deleted.
(blender/blender!117304) (blender/blender@0fd8f29e88)

![New purge popup](images/ui_new_purge_popup.png){style="width:500px;"}

The File > Cleanup menu has a new Manage Unused Data operation to open the outliner
in Unused Data mode. (blender/blender@f04bd961fd)

## Preferences

- New button to save custom themes. (blender/blender@ee38a7b723)

![Save theme](images/SaveTheme.png){style="width:500px;"}

- Improved operators for adding/deleting custom themes. (blender/blender@a0a2e7e0dd)
- Improved operators for adding/deleting custom key configurations. (blender/blender@57729aa8bb)
- Preferences "Translation" section changed to "Language". (blender/blender@73d76d4360)

## Splash

- Changes to Splash Screen for new versions. (blender/blender@d2d810f11a)

![Splash screen changes](images/SplashChanges.png){style="width:500px;"}

## Platforms

- Linux: Registering & unregistering file-type associations is now supported on Linux.
  This can be accessed via the system preferences "Operating System Settings" or via
  command line arguments (`--register`, `--register-allusers`), see `--help` for details.
  (blender/blender@9cb3a17352bd7ed57df82f35a56dcf0af85fbf03).
- Linux: Image copy and paste now works in Wayland. (blender/blender@692de6d380)
- Windows: Fixed pixel shift in clipboard image paste. (blender/blender@a677518cb5)
