# Blender 4.2: Sculpt, Paint, Texture

## Sculpting

### New Tools

- Polyline Mask
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)
- Lasso Hide
  (blender/blender@68afd225018e59d63431e02def51575bfc76f5cb)

<figure>
<video src="../../../videos/4.2_sculpt_lasso_hide.mp4"
       title="Demo of Lasso Hide tool" width="350" controls=""></video>
<figcaption>Lasso Hide</figcaption>
</figure>

- Line Hide
  (blender/blender@6e997cc75723b0dfc4d25d8246f373d46a662905)
- Polyline Hide
  (blender/blender@55fc1066acf69dd04f7f7b1c3af4c3f360769523)

<table class="md-typeset__table">
<tr>
<td>
<figure>
<video src="../../../videos/4.2_sculpt_line_hide.mp4"
       title="Demo of Line Hide tool" width="350" controls=""></video>
<figcaption>Line Hide</figcaption>
</figure>
</td>
<td>
<figure>
<video src="../../../videos/4.2_sculpt_polyline_hide.mp4"
       title="Demo of Polyline Hide tool" width="350" controls=""></video>
<figcaption>Polyline Hide</figcaption>
</figure>
</td>
</tr>
</table>

- Line Face Set
  (blender/blender@7726e7f563190620034f43a19aa01b9907a26530)
- Polyline Face Set
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)

- Line Trim
  (blender/blender@d4a61647bf571cb90090e1320f4aa1156482836f)
- Polyline Trim
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)
- Grow & Shrink Visibility
  (blender/blender@1ab3fffc1d5fa81d95f4d05a46d2f7ccf8fb1f93)
### New Options

- Choose between Fast and Exact solvers for Trim tools.
  ([documentation](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/box_trim.html#tool-settings))
  (blender/blender@881178895b8528fd37ef41cd7a4bc481f7b7d311)

## Other

- Sculpt and Weight Paint modes now use the global, customizable rotation
  increment for corresponding Line tools when snapping is enabled.
  For example Line Hide in Sculpt and Gradient in Weight Paint.
  (blender/blender@1cf0d7ca6a858b5c7a0b03a224d9e826ffa38c47)
