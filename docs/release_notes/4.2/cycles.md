# Blender 4.2: Cycles

## Ray Portals

The Ray Portal BSDF transports rays to another location in the scene, with specified ray position and normal. It can be used to render portals for visual effects, and other production rendering tricks. (blender/blender!114386)

See the [documentation](https://docs.blender.org/manual/en/4.2/render/shader_nodes/shader/ray_portal.html) for details and example node setups.

![Connecting two spaces through a portal](images/render_shader-nodes_ray-portal-bsdf_gateway-example.jpg)
![Simulating a camera feed](images/render_shader-nodes_ray-portal-bsdf_portal-to-screen-example.jpg)

## Shaders

* The Principled BSDF now supports physically accurate thin film interference effects for specular reflection and transmission. (blender/blender!118477)
* Principled Hair with the Huang model now provides more accurate results when viewing hairs close up, by dynamically switching between a near and far field model depending on the distance. (blender/blender!116094)
* Subsurface Scattering node now has a Roughness input, matching the Principled BSDF. (blender/blender!114499)

## Other

* Improved volume light sampling, particularly for spot lights and area light spread. (blender/blender!119438) (blender/blender!122667)
* World Override option for view layers. (blender/blender!117920)
* OpenImageDenoise is now GPU accelerated on AMD GPUs on Windows and Linux.
* OpenImageDenoise can now use GPU accelerated denoising for CPU renders. (blender/blender!118841)
* Intel GPU rendering now supports host memory fallback. (blender/blender!122385)
