# Blender 4.2: Core

## Undo

- Faster undo due to implicit sharing. (blender/blender@0e8e219d71cd27cf025a9920ac4fb54ff7c178b3)

## Portable Installation

- Creating a [portable installation](https://docs.blender.org/manual/en/4.2/advanced/blender_directory_layout.html#portable-installation) now works by creating a folder named `portable` next to the Blender executable, instead of a `4.2/config` folder. (blender/blender!122778)

## Environment Variables

- The new `BLENDER_SYSTEM_EXTENSIONS` environment variable adds extensions to a Blender installation.  See the [Deploying Blender in Production](http://docs.blender.org/manual/en/4.2/advanced/deploying_blender.html) documentation for details. (blender/blender!122832)
- The existing `BLENDER_SYSTEM_SCRIPTS` environment variable will now always add scripts likes legacy add-ons and presets to a Blender installation. This can be used for example in an animation studio to make scripts available to all users. Previously this environment variable often either did not work or behaved inconsistently. (blender/blender!122689)

## Command Line Arguments

- Running in background-mode using `-b` or `--background` now disables audio by default.
  (blender/blender@7c90018f2300646dbdec2481b896999fe93e6e62)
- New `--online-mode` and `--offline-mode` arguments to force Blender to enable or disable online access, overriding the preferences. (blender/blender!121994)