# Blender 4.2: Nodes & Physics

## Geometry Nodes
* The *Realize Instances* node now supports partially realizing the geometry (blender/blender@1db538683d94cc).
* Some sampling nodes now support a Group ID input to process the input geometry in multiple pieces.
  * *Sample Nearest Surface* (blender/blender@75e9056cac24d319bd5bb65dd13115ddad9074a9).
  * *Geometry Proximity* (blender/blender@0494605e7e422ad0980b35e4cc5f3703c02982ea).
* The *Remove Named Attribute* node now supports removing multiple attributes
  that have the same prefix or suffix
  (blender/blender@6c46178a7fe342ffc8a3c92a7d1cd42ee9483c21).
* The *Capture Attribute* node now supports capturing multiple attributes at once (blender/blender@28cef56ad2866993a4bd7ef1a4bb42da69dffaa6).
* The *Store Named Attribute* node can now write 8-bit integer attributes
  (blender/blender@1681e5511463).
* The rotation socket workflow is improved with more nodes and sockets.
  * The *Curve to Points* node now has a rotation instead of vector output socket
  (blender/blender@248dafef74be).
  * The new **Axes to Rotation** node simplifies creating a rotation based on two orthogonal axes. Usually those are a normal and a tangent (blender/blender@25c134fd081be52134bfad1f6ae6e05622917b14).
  * **Align Rotation to Vector** replaces the now-deprecated *Align Euler to Vector* node,
  with better performance and clearer socket types (blender/blender@462e7aeeddd1).
  * The new **Rotation** input node allows inserting a constant rotation in a node group (blender/blender@968b98be560f847d1cf).
* There have been a few user interface improvements specific to geometry nodes.
  * The sockets in the *Repeat* and *Simulation* zone and *Bake* node are now aligned
  (blender/blender@1f30f41af885aeb50c1d173a0303921a09dac421, blender/blender@5f02eaae9f6eb18d00f1272e33f5906489ded4fe).
  * The *Simulation* zone node now has an overlay showing which frames are baked (blender/blender@96db947f167e5fa746455798d6c2f7ab131bc3a3).
  * The socket list for some nodes is now in the properties panel instead of in an
  independent panel. This applies the Bake node and repeat zone, among others 
  (blender/blender@0c585a1b8af).
  * The *Menu Switch* node now has an "extend" socket (blender/blender@176c6ef329821006).
* The Face Neighbors mesh topology node now gives the correct number of unique faces
  (blender/blender@dd672c38b173f0274ac23251c87cd1d16f5869bd).
 
### Performance
* The *Scale Elements* node was rewritten to become at least 4-10x faster
  (blender/blender@2cb3677557b3).
* The *Sample UV Surface* node is 10-20x faster when used on large meshes
  (blender/blender@0a430fb8cbad3021dc1b969994a9a18bc59434d5).
* Many nodes (e.g. *Grid*) became faster due to general threading optimization
  (blender/blender@b99c1abc3ad6).

### Node Tools
* The **Mouse Position** gives access to the click position and a "Wait for Click" 
  option can delay the operator's execution until the user clicks
  (blender/blender@ce224fe4010a).
* The **Viewport Transform** node provides access to the view direction and location
  of the 3D viewport(blender/blender@83ed92d533f1).
* Data-block (collection, object, material, image) inputs are now supported,
  with the limitation that data-blocks from separate library files with non-unique
  names cannot be used (blender/blender@65803f262fc7).
* The new **Active Element** node makes the active vertex, edge, or face available to tools (blender/blender@2fe92c63d356).
* Extra object evaluations are now avoided, improving overall performance, especially when objects have modifiers
  (blender/blender@b3ecfcd77d6392005b773a5eaad4c0337a1c0e52).
* Socket inspection now works in the node editor for node tools
  (blender/blender@740d1fbc4b134a3ea11d67ccd2ed5b28ef27767d).

### Matrix Socket
* There is a new matrix socket type and a few corresponding nodes.
  * **Combine Transform**: Builds a matrix based on location, rotation and scale.
  * **Separate Transform**: Decomposes a matrix into its location, rotation and scale components.
  * **Transform Point**: Applies the matrix transformation on a point.
  * **Transform Direction**: Applies the matrix transformation on a direction. This ignores the
    location/translation component of the matrix.
  * **Project Point**: Applies the matrix on a point and also performs perspective divide.
  * **Combine Matrix**: Builds a 4x4 matrix from raw values (blender/blender@831e91c357131640ff10763fdc5220a3a16a290d).
  * **Separate Matrix**: Splits a 4x4 matrix into raw values (blender/blender@831e91c357131640ff10763fdc5220a3a16a290d).
  * **Set Instance Transform**: Assign a transformation matrix to each instance.
  * **Instance Transform**: Access the built-in `instance_transform` attribute (blender/blender@fb6b9092a7e19378effa3930526cb38d9b87b189).
  * **Invert Matrix**
  * **Multiply Matrices**
  * **Transpose Matrix**
* The *Object Info* node now has a Transform matrix output (blender/blender@4dfc1ede5836fff732e5d29be121fce4c3a7b8db).
* The *Transform Geometry* node now supports transforming with a matrix (blender/blender@9fcf97d9784f9a08fa0a66ca41a20382e92b6270).
* 4x4 matrices can now be stored as attributes.
* The *Accumulate Field* node now supports accumulating transform matrices (blender/blender@78c1c1a1705eef442b06ea3f991d54f74fc9566a).

## Node Editor
* Input sockets that don't allow editing the value don't show up in the sidebar anymore.
  (blender/blender@d27a1c47fafb84ebf0649423102a9e79b9a201b7,
  blender/blender@d9d9ff1dcd2036997064f982bbb94cf7d52e4828).
* Slightly improved sorting in search when adding nodes (blender/blender@d1634b2a4aae,
  blender/blender@22c6831f63ad).
* Show tooltip with node description when over node title (blender/blender@749433f20bc5bd893fee0c9fcb2d20578a67d224).
* Improved tooltips for dangling reroute nodes (blender/blender@fa66b52d0ae6).
* Improved tooltips for multi-input sockets (blender/blender@ed9921185abb).
* Display node label for tooltips on reroute nodes (blender/blender@0bd627950436).
* Multiple images can be added at the same time using drag and drop (blender/blender@615100acdaba).
* Node groups can have descriptions now (blender/blender@6176e66636342dabbd78594ecfb188c8a4bc996c).
* Node group properties have been reorganized (blender/blender@125afb20d330a27a51b73cd4f55cd4136ea71fbb).
* Node groups now have a color tag that affects the header color of group nodes (blender/blender@f4b9ca758a10dbc295f608f1a56c97397c8dd884).
* When adding a frame around the selected nodes (ctrl+J), the new frame will now be added to the common root frame of these nodes (blender/blender@0f32291d15ef2e076adcb63eec5aa32681b548e7).
* Invalid (red) links now have some additional information for why they are invalid (blender/blender@399f6a1c6056bee6fd4f311baee66e69ba2b2a88).
* Dragging a node onto a link does not remove the link anymore if it is incompatible. It's dimmed instead (blender/blender@ebb61ef30f716918592c310568b2d154dfde72a4).
* Resetting socket values now actually works, instead of always setting values to zero (blender/blender@6b6657405f69c86e280d3d0bf5547b9834d39f6e).
* Using the shortcut (ctrl+shift+click) to connect to a viewer now also moves the viewer closer to the node (blender/blender@7be4d4f443a82a589ec47238765fc16aa9934246).
* Some sockets are can now be renamed directly in the node instead of in the sidebar by ctrl+clicking on it (blender/blender@cd5eb4aa03ee93279d16d4f36ca09a2fc7717dff).
* New overlay to reroute labels that are derived from upstream reroute nodes (blender/blender@db5d41016446467be11174a7e74a9f836fea4826).
