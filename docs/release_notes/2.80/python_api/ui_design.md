With the forthcoming release of Blender 2.8, addons need to be updated.
This document serves as a guide to addon developers on how to integrate
with the Blender’s UI in 2.8.

## Old addons

In 2.7x, there was a convention to put all addons inside the toolbar
(T-key). This was somewhat a random accident of history, in that the
toolbar was always just a list of duplicated quick commands. Because of
the new way tools are handled in 2.8, this may no longer be appropriate.

Depending on what type of addon you are making, you should integrate
into the 2.8 UI in different ways. Below are several scenarios that
illustrate what to do in common cases.

## Commands

If your addon is simply an operator command (eg such as Subdivide,
Symmetrize, Convert, Join etc.) there is no need to add a panel or a
button at all. Simply register and add your command into the appropriate
menu inside the correct viewport and mode.

*Examples:*

- Object mode selection operator should be placed in the *3D View* \>
  *Object Mode* \> *Object* menu
- Object mode mesh editing operator should be placed in the *3D View* \>
  *Edit Mode* \> *Vertex*, *Edge* or *Face* menu, depending on which is
  the appropriate

You can also register and add your command to the contextual menu (right
click menu)

For quick access, users can add your command to their custom Quick
Commands menu (Q key)

## Tools

If your addon makes sense as an active tool (anything that is
interactive inside the viewport), your addon can register itself as an
active tool in the toolbar.

*Examples:*

- Active tool to interactively add objects by dragging
- Active tool to interactively bend or curve a mesh

The settings pertaining to the active tool will automatically be added
to the Tool Properties section.

### Toolbar Icons

Tools in the toolbar have to supply an icon. Here's how:

In blender 2.8 we have the possibility of creating binary icon files
(.dat) from blender files (.blend). You can see some examples of these
blender files with icons in the "icon_geom.blend" in the library with
the blender dependencies. (..lib/resources/icon_geom.blend).

This library is located in Blender's Resources SVN repository, here:

<https://svn.blender.org/svnroot/bf-blender/trunk/lib/resources>

To create the actual icons, follow the built-in README.txt inside
icon_geom.blend. This will guide you through creating the icon mesh
correctly.

Once you do the geometry in blender, you can transform it into a binary
icon file with the command:

`  ./blender.bin `  
`      icon_file.blend --background --python ./release/datafiles/blender_icons_geom.py -- `  
`      --output-dir=./release/datafiles/blender_icons_geom`

## Properties

Perhaps your addon is best described as a list of custom properties that
attach themselves to a data ID block. If this is the case, consider
adding a panel inside the Properties Editor.

## Templates

Addons may add a custom template, which alters the way Blender looks and
functions. This may be appropriate for addons that intend to completely
change the focus of Blender, such as a 3D-printing addon, or
architecture addon.

*Examples:*

- Architecture modeling addon
- Children-focused extra simple version of Blender

## All of the Above

Addons don't have to fall strictly into any of the categories outlined
above. They can combine any of the above approaches in the same addon.
