# Blender 2.8: View Layers and Collections

## Collections

Layers and groups have been replaced by *collections*. These are used
for organizing objects in a scene, instancing groups of objects, and
linking groups of objects across .blend files.

![Collections and objects in the outliner](../../images/Blender2.80_collections_example.png){style="width:450px;"}

Objects in a scene can be organized into named and nested collections.
Objects are typically a member of one collection in a scene, but they
can be put in multiple collections as well.

Collections do not have to be part of a scene, and can also exist on
their own. This is useful for example to limit physics collision to a
subset of objects in the scene, or to select a number of objects to
instance with a particle system.

### Backwards Compatibility

The new system is backwards compatible with older files that contain
layers and groups. For each of the previous 20 unnamed scene layers, a
collection is created if the layer contained any object. These
collections are made part of the scene. Groups are converted to
collections as well, which continue to exist not attached to one
particular scene.

## Outliner

The default outliner view shows all the collection that are part of the
scene. Collections and objects can be organized by using drag and drop.

To view the full list of collections that exist in the file, the Blender
File view of the outliner can be used.

![All collections in the file](../../images/Blender2.80_collections_all.png){style="width:450px;"}

## 3D Viewport

In the 3D viewport, objects can be moved to collections quickly with the
updated M key menu. A new collection can be immediately created from the
same menu as well.

![Move menu](../../images/Blender2.80_collections_move.png)

## Visibility

Hiding of objects is done with the H, Shift+H and Alt+H key shortcuts.
Unlike before, this is a more temporary state and purely intended as a
quick way to hide or isolate objects while working. Objects hidden this
way are still part of the scene and evaluated, and so they still affect
playback performance.

In the 3D viewport, collections can be hidden or isolated quickly using
either the Collections panel in the sidebar, or with the equivalent
Ctrl+H menu. These show numbers next to the collection name, and
pressing the corresponding numbers on the keyboard can be used to
isolate the corresponding collection quickly. Objects can also be hidden
or made non-selectable per type, using settings in the 3D viewport
header.

There are also settings to more persistently enable or disable
collections and objects, for either the viewport or renders. These
settings are preserved when linking collections and objects to other
files. Objects and collections disabled this way are fully excluded from
the scene and will not affect performance. These settings are edited in
the outliner, and made visible through the Filter options.

<div style="clear: both">
</div>

## View Layers

Render layers have been renamed to view layers, to indicate their
expanded scope. Besides splitting up a render into multiple layers for
compositing, they can now also be used as multiple views and variations
of a scene for editing. Different windows can show different view
layers, by selecting the scene and view layer in the top bar.

Collections can be excluded from specific view layers with the checkmark
left of the name. They can also set to contribute only indirect light
and shadows, or act as a holdout mask. These settings are edited in the
outliner now.

![Outliner visibility settings display filter](../../images/Blender2.80_visibility_settings.png)
