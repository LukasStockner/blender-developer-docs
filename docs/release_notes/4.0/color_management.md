# Blender 4.0: Color Management

## AgX View Transform

The AgX view transform has been added, and replaces Filmic as the
default in new files.
(blender/blender@a9053f7efbe8b25429b11bccd679dbe81ea60e3f)

This view transform provides better color handling in over-exposed areas
compared to Filmic. In particular bright colors go towards white,
similar to real cameras. Technical details and image comparisons can be
found in blender/blender!106355.

|Filmic|AgX|
|-|-|
|![](../../images/Render4.0-einar-filmic.jpeg)|![](../../images/Render4.0-einar-agx.jpeg)|
|![](../../images/Render4.0-stray-filmic.jpg)|![](../../images/Render4.0-stray-agx.jpg)|
|![](../../images/Render-4.0-circle-filmic.jpeg)|![](../../images/Render-4.0-circle-agx.jpeg)|

Scenes by Eary Chow, Alaska Young and Leonard Siebeneicher. Using assets
from by Blender Studio, AmbientCG and Poly Haven.

## HDR Display on macOS

A new High Dynamic Range option was added in the Color Management \>
Display panel. It enables display of extended color ranges above 1.0 for
the 3D viewport, image editor and render previews.
(blender/blender@2367ed2ef241846d99fc8bab73abad4d48b9e67b)

This requires a monitor that can display HDR colors, and a view
transform designed for HDR output. The Standard view transform works,
but Filmic and AgX do not as they were designed to bring values into the
0..1 range for SDR displays.

## New Color Spaces

New linear spaces and display devices were added, together with some
renaming to improve clarity. Forward compatibility code is included into
Blender 3.6.
(blender/blender@6923f7a1539)

## Removed Features

- Textures and other areas of Blender now always considers the color
  management is enabled. This used to be a compatibility option for the
  "No Color Management" setting in Blender pre 2.64. To achieve the same
  functionality the images need to be set to Non-Color space.
  (blender/blender@63e2832057)
- Unused color spaces and display devices were removed from the OCIO
  configuration
  (blender/blender@b2b7b37139)
- The XYZ display was only used to output images in XYZ space, for which
  there is now an option in the image output settings. Instead of the
  None display use Raw vie of the sRGB display. There is a versioning
  code in place to migrate the display settings to the new notation.
