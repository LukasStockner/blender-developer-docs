# Tooling

## C/C++

- [ClangFormat](clangformat.md): automatic code formatting following Blender style
- [Address Sanitizer](asan.md): detect memory errors on Linux and macOS
- [Valgrind](valgrind.md): detect memory errors on Linux memory errors (Linux/macOS only)
- [GDB](gdb.md): debugging on Linux
- [Doxygen](doxygen.md): code documentation viewing and editing

## Python

- [Profiling](python_profile.md): How to profile Python function calls.
- [Tracing](python_trace.md): How to log script execution.
- [Py from C](pyfromc.md): run Python scripts in C code (test changes without rebuilds).

## Building

- [BuildBot](buildbot.md): automated builds at [builder.blender.org](http://builder.blender.org)
- [CMake for Developers](../building_blender/options.md#setup-for-developers): CMake options for development
- [Unity Builds](unity_builds.md): reduce compile times
- [distcc](distcc.md): distributed building

## Utilities

- [Blender Tools](blender_tools.md): handy scripts in the Blender repository
- [Git Bisect with Event Simulation](git_bisect_with_event_simulation.md): automatically find faulty commits
- [tea](tea.md): command line tool for interacting with Gitea

## Version Control

- [Git](../contributing/using_git.md): version control for all projects
- [Subversion](../contributing/subversion.md): legacy version control for old Blender versions

## Services

There is an [issue tracker](https://projects.blender.org/infrastructure/blender-projects-platform/issues)
for bugs and plans in development web services like Gitea and Buildbot.
