# Third Party Licenses

An important part of the [release process](index.md) is to
make sure [is shipped with the complete license for all its
dependencies](https://code.blender.org/2021/05/blender-oss-and-licenses/Blender).

Those dependencies come from a few places:

- extern
- intern
- SVN libraries

It is not enough to list the licences (MIT, GPL, ...). Often times the
license also require its copyright holders to me mentioned. To help
organize the process Blender uses [OSS Attribution
Builder](https://projects.blender.org/blender/oss-attribution-builder).

This web application allow individual packages to be listed, together
with their license and copyright notes. It then export a
[THIRD-PARTY-LICENSES.txt](https://projects.blender.org/blender/blender/src/branch/main/release/license/THIRD-PARTY-LICENSES.txt)
document that is shipped together with Blender.

## Release

Every release is kept as a separate project. Each release we update the
individual packages versions, as well as adding new ones. So the best
way to start is by cloning an existing project.

![](../../../images/Oss-attribute-license-2.png)

It is important to check the new libraries in all the places (intern,
extern, svn), create new entries or update the existing one.

## Backup

For every release, a dump of the database should be pushed to the
[repository](https://projects.blender.org/blender/oss-attribution-builder/src/branch/main/pgdata/sql-dump.sql).

- `docker-compose up`
- Then in a different terminal:
- `./utils/make_database_backup.sh`
- `cp pgdata/dumps/pgsql-dump-\$(date +"%Y%m%d")\*.pgsql pgdata/sql-dump.sql`
- `git add pgdata/sql-dump.sql`
- `git commit -m'Blender 3.2 database dump'`

## What Changed ?

Usually there will be a "[Libraries Changes for Blender
3.5](https://projects.blender.org/blender/blender/issues/99618)" for you
to use as a base.

!!! Note "Trust but verify"
    That document only tells part of the story!

The ground truth for the change is:

- intern (intern/CMakeLists.txt)
- extern (build_files/build_environment/cmake/versions.cmake)
- patches (build_files/build_environment)

For intern if you need more details about the changes you can do:
`git diff --stat origin/blender-v3.1-release...origin/blender-v3.2-release --
intern ':!intern/cycles'`

Patches are mostly relevant to mark whether the source is modified or
not.
