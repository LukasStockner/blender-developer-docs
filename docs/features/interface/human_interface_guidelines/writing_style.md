# Writing Style

### Common Rules

- **Use American English.**

- **Keep text short and to the point.**

- **Capitalize titles.** Entity names are not titles.
  - Do <u>not</u> capitalize *mesh*, *object*, *vertex group*, etc.
  - Not capitalizing entity names is an arguable guideline, but avoids
    many fuzzy cases.

- Do <u>not</u> use abbreviations like _verts_, _VGroups_, _loc_, or _rot_,
  _fac_. Instead, use plain words like _vertices_, _vertex groups_,
  _location_, _rotation_, _factor_.

- Do <u>not</u> use English contractions like _aren’t_, _can’t_, etc. It's
  preferred to keep full spelling, _are not_ or _cannot_ are not that
  much longer, and it helps keeping consistent styling over the whole
  UI.

- “Channel” identifiers, like _X_, _Y_, _Z_, _R_, _G_, _B_, etc. are always capitalized.

- Do <u>not</u> use implementation specific language.

  !!! Example "Examples"
      - Instead of _ID_, use _data-block_.
      - Do <u>not</u> use terms like _flag_, _string_, _RNA_, _DNA_.

- Don't use pronouns, such as _you_. This sounds like an accusation towards the user.<br/>

  !!! Example
      **Don't**: _Your mesh is very dense_<br/>
      **Do**: _The mesh is very dense_

- Use the ampersand (_&_) in labels, but _and_ in text (e.g. in tooltip descriptions).

- Text should generally <u>not</u> contain code snippets, or involved details (e.g.
  troubleshooting, corner cases that might not work, etc.).

Formatting with new lines (`\n`) and bullet points (`\u2022`) is fine.

### UI Labels

- **Must use English MLA title case** (see [this
  overview](https://titlecaseconverter.com/rules/#MLA), also
  [\#79589](http://developer.blender.org/T79589)).

  !!! Example "Examples"
      - _Like in This Example_
      - _Set Offset from Cursor_
      - _Alexander and the Terrible, Horrible, No Good, Very Bad Day_
      - _Be Careful What You Wish For_
  Tip: There are various online converters available.

- An operation that opens a new popup or window should end with an ellipsis ("...").
- **Avoid redundancy.** For example do <u>not</u> use _Enable_, _Activate_, _Is_, _Use_, or similar
  words for boolean properties (e.g. labels in checkboxes).

  !!! Example
      **Don't**: _Use Transparency_<br/>
      **Do**: _Transparency_
