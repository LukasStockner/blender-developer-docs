# BLI: Blender Library

Core low-level data structures used throughout Blender. The documentation here is supposed to give a birds eye view over the available data structures and when to use them. More details for all available methods can be found in the source code.
