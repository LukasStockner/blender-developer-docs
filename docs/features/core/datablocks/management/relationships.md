# Relationships Between IDs

## Basics

- Relationship to another ID is done through pointers.
- Relation ship can be strong (implying user refcounting increment of
  the used ID), or shallow. Strong relationships will ensure that the
  used IDs remain available in the .blend file, while shallow
  relationships do not prevent an automated deletion of the dependency.

## Looping over ID usages

The
[BKE_lib_query.h](https://projects.blender.org/blender/blender/src/branch/main/source/blender/blenkernel/BKE_lib_query.h)
module provides an API to loop over all other IDs usages of a given ID
(or for all IDs in the whole Main data-base).

For each ID pointer, a user-defined callback is called, with the
`self` ID owner of the pointer, the used ID pointer itself, and some
meta information (is this ID pointer a 'user refcounting' usage or not,
etc.).

This code is one of the core pieces of ID management, as it allows
generic processing of all ID relationships. It is used to ensure or
validate user refcounting, safely delete a data-block, replace a
data-block by another, build temporary representations of the ID
relationships, etc.
